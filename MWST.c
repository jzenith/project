#include <stdio.h>
#include <stdlib.h>

typedef struct Edge{
    int src, index, dst;
    double weight;
} Edge;

typedef struct Graph{
    Edge* e;
    int V, E;
} Graph;

Graph* initG(int V, int E);
int search(int p[], int i);
void join(int p[], int x, int y);
int comp(const void* a, const void* b);
void mwst(Graph* graph, FILE *out);


int main(int argc, char* argv[]) {
    if(argc != 3){
        printf("Wrong number of args");
        exit(EXIT_FAILURE);
    }

    FILE* in = fopen(argv[1], "r");
    if (in == NULL) {
        printf("Unable to open input file %s\n", argv[1]);
        exit(EXIT_FAILURE);
    }
    FILE* out = fopen(argv[2], "w");
    if (out == NULL) {
        printf("Unable to open output file %s\n", argv[2]);
        exit(EXIT_FAILURE);
    }

    int n, m, u, v, w;
    fscanf(in, "%d", &n); //vertices 
    fscanf(in, "%d", &m); //edges 


    Graph* G = initG(n, m);

    int i = 0;
    while (fscanf(in, "%d %d %d", &u, &v, &w) == 3) {
        G->e[i].src = u;
        G->e[i].dst = v;
        G->e[i].weight = w;
        G->e[i].index = (i+1);
        i++;
    }

    mwst(G, out);

    return 0;
}


Graph* initG(int V, int E) {
    Graph* G = (Graph*)malloc(sizeof(Graph));
    G->V = V;
    G->E = E;
    G->e = (Edge*)malloc(E * sizeof(Edge));
    return G;
}


int search(int p[], int i) {
    if (p[i] == -1)
        return i;
    return search(p, p[i]);
}
void join(int p[], int x, int y) {
    p[x] = y;
}

int comp(const void* a, const void* b) {
    Edge* a1 = (Edge*)a;
    Edge* b1 = (Edge*)b;
    //printf("\n%s%s%d%s%d%.2f%s%s%d%s%d%.2f\n", "Weights: ", "V: ", a1->src, "-", a1->dst, a1->weight, " : ", "V: ", b1->src, "-", b1->dst, b1->weight);
    printf(" ");
    if (a1->weight >= 0 && b1->weight >= 0)
        return a1->weight > b1->weight;
    else 
        return a1->weight > b1->weight;
    /*
    else if(a1->weight < 0 && b1->weight >= 0)
        return a1->weight > b1->weight;
        //return b1->weight > a1->weight;
    else if(a1->weight >= 0 && b1->weight < 0)
        return a1->weight > b1->weight;
        //return b1->weight > a1->weight;
    else //(a1->weight < 0 && b1->weight < 0)
        return a1->weight > b1->weight;
        //return b1->weight > a1->weight;
    */
}



void mwst(Graph* g, FILE *out) {
    int V = g->V;
    Edge rslt[V];  
    int k = 0;  
    int i = 0;  

    qsort(g->e, g->E, sizeof(g->e[0]), comp);

    int* p = (int*)malloc(V * sizeof(int));

    for (int v = 0; v < V; ++v)
        p[v] = -1;

    while (k < V - 1 && i < g->E) {
        Edge adjEdge = g->e[i++];

        int x = search(p, adjEdge.src);
        int y = search(p, adjEdge.dst);

        if (x != y) {
            rslt[k++] = adjEdge;
            join(p, x, y);
        }
    }

    double tw = 0;
    for (i = 0; i < k; ++i){
        printf("%d: (%d, %d) %.1f\n", rslt[i].index, rslt[i].src, rslt[i].dst,
               rslt[i].weight);
        fprintf(out, "%d: (%d, %d) %.1f\n", rslt[i].index, rslt[i].src, rslt[i].dst,
               rslt[i].weight);
        tw += rslt[i].weight;
    }
    printf("%s%.2f", "Total Weight = ", tw);
    fprintf(out, "%s%.2f", "Total Weight = ", tw);
    return;
}
